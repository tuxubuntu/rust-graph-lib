use std;
use std::error;
use std::fmt;

pub type Result<T> = std::result::Result<T, GraphError>;

#[derive(Debug)]
pub struct GraphError( pub &'static str );

impl fmt::Display for GraphError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		write!(f, "Graph Error: {}", self.0)
	}
}

impl error::Error for GraphError {
	fn description(&self) -> &str {
		self.0
	}
	fn cause(&self) -> Option<&error::Error> {
		None
	}
}

