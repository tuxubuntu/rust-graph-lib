#[cfg(test)]

#[test]
fn getters() {
	use *;
	use super::Initiated;
	let mut graph = GraphBasic::new();

	let n0 = graph.make();

	let n1 = graph.get_mut(&n0).unwrap().make();

	let _n2= graph.get_mut(&n0).unwrap().make();

	let _n3= graph.get_mut(&n0).unwrap().get_mut(&n1).unwrap().make();
	let _n4= graph.get_mut(&n0).unwrap().get_mut(&n1).unwrap().make();
	let n5 = graph.get_mut(&n0).unwrap().get_mut(&n1).unwrap().make();

	graph
		.get_mut(&n0).unwrap()
		.get_mut(&n1).unwrap()
		.get_mut(&n5).unwrap()
		.payload_set(true);

	let res = *graph
		.get(&n0).unwrap()
		.get(&n1).unwrap()
		.get(&n5).unwrap()
		.payload_get().unwrap();

	assert_eq!(res, true);
}

/*
 *      (0)
 *      / \
 *    (1) (2)
 *    /|\
 *   / | \
 * (3)(4)(5)
 * 
 * 5 is true
 * 
 */
