
pub trait Graph<C, I> {
	fn add(&mut self, C) -> I;
	fn len(&self) -> usize;
	fn get(&self, &I) -> Option<&C>;
	fn get_mut(&mut self, &I) -> Option<&mut C>;
	fn children(&self) -> &Vec<C>;
}

pub trait Payloaded<T, C, I> where Self: Sized+Graph<C, I> {
	fn payload_get(&self) -> Option<&T>;
	fn payload_mut(&mut self) -> Option<&mut T>;
	fn payload_set(&mut self, T);
	fn from_payload(T) -> Self;
	fn make_from(&mut self, T) -> I;
}

pub trait Initiated<C, I> where Self: Sized+Graph<C, I> {
	fn new() -> Self;
	fn make(&mut self) -> I;
}

#[derive(Debug, Clone)]
pub struct GraphBasic<T> {
	children: Vec<GraphBasic<T>>,
	payload: Option<T>,
}

impl<T> Initiated<GraphBasic<T>, usize> for GraphBasic<T> {
	fn new() -> GraphBasic<T> {
		GraphBasic {
			children: Vec::new(),
			payload: None,
		}
	}
	fn make(&mut self) -> usize {
		let graph = GraphBasic::new();
		self.add(graph)
	}
}

impl<T> Graph<GraphBasic<T>, usize> for GraphBasic<T> {
	fn add(&mut self, graph: GraphBasic<T>) -> usize {
		let id = self.children.len();
		self.children.push(graph);
		id
	}
	fn len(&self) -> usize {
		self.children.len()
	}
	fn get(&self, id: &usize) -> Option<&GraphBasic<T>> {
		self.children.get(*id)
	}
	fn get_mut(&mut self, id: &usize) -> Option<&mut GraphBasic<T>> {
		self.children.get_mut(*id)
	}
	fn children(&self) -> &Vec<GraphBasic<T>> {
		&self.children
	}
}

impl<T> Payloaded<T, GraphBasic<T>, usize> for GraphBasic<T> {
	fn payload_get(&self) -> Option<&T> {
		match self.payload {
			Some(ref payload) => Some(payload),
			None => None,
		}
	}
	fn payload_mut(&mut self) -> Option<&mut T> {
		match self.payload {
			Some(ref mut payload) => Some(payload),
			None => None,
		}
	}
	fn payload_set(&mut self, payload: T) {
		self.payload = Some(payload);
	}
	fn from_payload(payload: T) -> GraphBasic<T> {
		let mut graph = GraphBasic::new();
		graph.payload_set(payload);
		graph
	}
	fn make_from(&mut self, payload: T) -> usize {
		let graph = GraphBasic::from_payload(payload);
		let id = self.children.len();
		self.add(graph);
		id
	}
}
